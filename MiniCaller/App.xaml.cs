﻿using Microsoft.Shell;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Controls;
//using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;

namespace MiniCaller
{
	/// <summary>
	/// Interaction logic for App.xaml
	/// </summary>
	public partial class App : Application, ISingleInstanceApp
	{
		[STAThread]
		public static void Main()
		{
			//AppDomain.CurrentDomain.AssemblyResolve
			if (SingleInstance<App>.InitializeAsFirstInstance("MiniCaller"))
			{
				var application = new App();

				application.InitializeComponent();
				//AppDomain.CurrentDomain.AssemblyResolve += OnResolveAssembly;
				AppDomain.CurrentDomain.AssemblyResolve += CurrentDomain_AssemblyResolve;
				application.Run();

				// Allow single instance code to perform cleanup operations
				SingleInstance<App>.Cleanup();
			}
			//else
			//{
			//	NativeMethods.PostMessage(
			//					(IntPtr)NativeMethods.HWND_BROADCAST,
			//					NativeMethods.WM_SHOWME,
			//					IntPtr.Zero,
			//					IntPtr.Zero);
			//}
		}

		private static Assembly CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs args)
		{
			String dllName = new AssemblyName(args.Name).Name + ".dll";
			var assem = Assembly.GetExecutingAssembly(); 
			String resourceName = assem.GetManifestResourceNames().FirstOrDefault(rn => rn.EndsWith(dllName));
			if (resourceName == null) 
				return null; // Not found, maybe another handler will find it    
			using (var stream = assem.GetManifestResourceStream(resourceName)) {       Byte[] assemblyData = new Byte[stream.Length];        stream.Read(assemblyData, 0, assemblyData.Length);        return Assembly.Load(assemblyData);    } 
		}
		//System.Reflection.Assembly CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs args)
		//{
		//	String dllName = new AssemblyName(args.Name).Name + ".dll";
		//	var assem = Assembly.GetExecutingAssembly();
		//	String resourceName = assem.GetManifestResourceNames().FirstOrDefault(rn => rn.EndsWith(dllName));
		//	if (resourceName == null) return null; // Not found, maybe another handler will find it    
		//	using (var stream = assem.GetManifestResourceStream(resourceName))
		//	{
		//		Byte[] assemblyData = new Byte[stream.Length];
		//		stream.Read(assemblyData, 0, assemblyData.Length);
		//		return Assembly.Load(assemblyData);
		//	}
		//}
		//private static Assembly OnResolveAssembly(object sender, ResolveEventArgs args)
		//{
		//	Assembly executingAssembly = Assembly.GetExecutingAssembly();
		//	AssemblyName assemblyName = new AssemblyName(args.Name);
		//	string path = assemblyName.Name + ".dll";
		//	if (assemblyName.CultureInfo.Equals(CultureInfo.InvariantCulture) == false)
		//	{
		//		path = String.Format(@"{0}\{1}", assemblyName.CultureInfo, path);
		//	}
		//	using (Stream stream = executingAssembly.GetManifestResourceStream(path))
		//	{
		//		if (stream == null)
		//			return null;
		//		byte[] assemblyRawBytes = new byte[stream.Length];
		//		stream.Read(assemblyRawBytes, 0, assemblyRawBytes.Length);
		//		return Assembly.Load(assemblyRawBytes);
		//	}

		//}


		protected override void OnStartup(StartupEventArgs e)
		{
			EventManager.RegisterClassHandler(typeof(TextBox), TextBox.PreviewMouseLeftButtonDownEvent,
						new MouseButtonEventHandler(SelectivelyIgnoreMouseButton));
			EventManager.RegisterClassHandler(typeof(TextBox), TextBox.GotKeyboardFocusEvent,
					new RoutedEventHandler(SelectAllText));
			EventManager.RegisterClassHandler(typeof(TextBox), TextBox.MouseDoubleClickEvent,
					new RoutedEventHandler(SelectAllText));
			//AppDomain.CurrentDomain.AssemblyResolve += CurrentDomain_AssemblyResolve;
			base.OnStartup(e);
		}
		public void SelectivelyIgnoreMouseButton(object sender, MouseButtonEventArgs e)
		{
			// Find the TextBox
			DependencyObject parent = e.OriginalSource as UIElement;
			while (parent != null && !(parent is TextBox))
				parent = VisualTreeHelper.GetParent(parent);

			if (parent != null)
			{
				var textBox = (TextBox)parent;
				if (!textBox.IsKeyboardFocusWithin)
				{
					// If the text box is not yet focused, give it the focus and
					// stop further processing of this click event.
					textBox.Focus();
					e.Handled = true;
				}
			}
		}

		public void SelectAllText(object sender, RoutedEventArgs e)
		{
			var textBox = e.OriginalSource as TextBox;
			if (textBox != null)
				textBox.SelectAll();
		}

		public bool SignalExternalCommandLineArgs(IList<string> args)
		{
			this.MainWindow.WindowState = WindowState.Normal;
			return true;
		}
	}
	//internal class NativeMethods
	//{
	//	public const int HWND_BROADCAST = 0xffff;
	//	public static readonly int WM_SHOWME = RegisterWindowMessage("WM_SHOWME");
	//	[DllImport("user32")]
	//	public static extern bool PostMessage(IntPtr hwnd, int msg, IntPtr wparam, IntPtr lparam);
	//	[DllImport("user32")]
	//	public static extern int RegisterWindowMessage(string message);
	//}
}
