﻿using AxCTILINKLib;
using MahApps.Metro.Controls;
using MiniCaller.Properties;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MiniCaller
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : MetroWindow
	{
		#region ctor
		public MainWindow()
		{
			InitializeComponent();
			//AppDomain.CurrentDomain.AssemblyResolve += CurrentDomain_AssemblyResolve;
			Loaded += Window_Loaded;
			SearchTextBox.AddHandler(TextBox.MouseLeftButtonDownEvent, new MouseButtonEventHandler(search_click), true);
			startup = DateTime.Now;
			lb.ItemsSource = log;
			MinimizeToTray.Enable(this);
			FeedElb();
			//emailTb.DataContext = new EmailAddress() { Email = "dummy@dummy.com" };
			//sendButton.DataContext = new 
			//incidentTb.DataContext = new IncidentNumber() { Number = string.Empty };

		}

		//Assembly CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs args)
		//{
		//	MessageBox.Show(args.Name);
		//	return null;
		//}


		//protected override void OnSourceInitialized(EventArgs e)
		//{
		//	base.OnSourceInitialized(e);
		//	HwndSource source = PresentationSource.FromVisual(this) as HwndSource;
		//	source.AddHook(WndProc);
		//}

		//private IntPtr WndProc(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
		//{
		//	if (msg == NativeMethods.WM_SHOWME)
		//	{
		//		ShowMe();
		//	}

		//	return IntPtr.Zero;
		//}


		//private void ShowMe()
		//{
		//	if (this.WindowState == System.Windows.WindowState.Minimized)
		//	{
		//		this.WindowState = System.Windows.WindowState.Normal;
		//		Activate();
		//	}
		//}

		private void search_click(object sender, MouseButtonEventArgs e)
		{
			if (focused && empGrid.Visibility == System.Windows.Visibility.Collapsed)
			{
				//empGrid.Visibility = System.Windows.Visibility.Visible;
				//logGrid.Visibility = System.Windows.Visibility.Collapsed;
				Fade(logGrid, empGrid);
			}
		}


		private void FeedElb()
		{
			elb.Items.Add(new Employee() { FullName = "刘锋", PinYin = "lf", HotLine = "8001", InternalLine = "2294" });
			elb.Items.Add(new Employee() { FullName = "徐鸿至", PinYin = "xhz", HotLine = "8002", InternalLine = "2105" });
			elb.Items.Add(new Employee() { FullName = "江民彬", PinYin = "jmb", HotLine = "8003", InternalLine = "2007" });
			elb.Items.Add(new Employee() { FullName = "李可臻", PinYin = "lkz", HotLine = "8004", InternalLine = "2097" });
			elb.Items.Add(new Employee() { FullName = "马克玲", PinYin = "mkl", HotLine = "8005", InternalLine = "2224" });
			elb.Items.Add(new Employee() { FullName = "张赛", PinYin = "zs", HotLine = "8006", InternalLine = "2107" });
			elb.Items.Add(new Employee() { FullName = "刘宝付", PinYin = "lbf", HotLine = "8007", InternalLine = "2126" });
			elb.Items.Add(new Employee() { FullName = "刘卓颖", PinYin = "lzy", HotLine = "8008", InternalLine = "2251" });
			elb.Items.Add(new Employee() { FullName = "慕晓燕", PinYin = "mxy", HotLine = "8009", InternalLine = "2096" });
			//elb.Items.Add(new Employee() { FullName = "邓书斌", PinYin = "dsb", HotLine = "8010", InternalLine = "2248" });
			elb.Items.Add(new Employee() { FullName = "朱政", PinYin = "zz", HotLine = "8011", InternalLine = "2226" });
			elb.Items.Add(new Employee() { FullName = "李少华", PinYin = "lsh", HotLine = "8012", InternalLine = "2101" });
			elb.Items.Add(new Employee() { FullName = "朱新颖", PinYin = "zxy", HotLine = "8013", InternalLine = "2042" });
			elb.Items.Add(new Employee() { FullName = "王知方", PinYin = "wzf", HotLine = "8015", InternalLine = "2195" });
			elb.Items.Add(new Employee() { FullName = "秦桐", PinYin = "qt", HotLine = "8016", InternalLine = "2102" });
			elb.Items.Add(new Employee() { FullName = "石羽", PinYin = "sy", HotLine = "8017", InternalLine = "2099" });
			elb.Items.Add(new Employee() { FullName = "张春峰", PinYin = "zcf", HotLine = "8018", InternalLine = "2103" });
			elb.Items.Add(new Employee() { FullName = "陈勃圣", PinYin = "cbs", HotLine = "8019", InternalLine = "2095" });
			elb.Items.Add(new Employee() { FullName = "何超", PinYin = "hc", HotLine = "8022", InternalLine = "2108" });
			elb.Items.Add(new Employee() { FullName = "穆天龙", PinYin = "mtl", HotLine = "8029", InternalLine = "2038" });
			elb.Items.Add(new Employee() { FullName = "IT", PinYin = "it", HotLine = "", InternalLine = "8000" });
			elb.Items.Add(new Employee() { FullName = "张涛", PinYin = "zt", HotLine = "", InternalLine = "2062" });
			elb.Items.Add(new Employee() { FullName = "陈文杰", PinYin = "cwj", HotLine = "", InternalLine = "2219" });
			elb.Items.Add(new Employee() { FullName = "北京办", PinYin = "bjb", HotLine = "010-57632288", InternalLine = "" });
			elb.Items.Add(new Employee() { FullName = "上海办", PinYin = "shb", HotLine = "021-64268423", InternalLine = "" });
			elb.Items.Add(new Employee() { FullName = "广州办", PinYin = "gzb", HotLine = "020-86007565", InternalLine = "" });
			elb.Items.Add(new Employee() { FullName = "成都办", PinYin = "cdb", HotLine = "028-86080839", InternalLine = "" });
			elb.Items.Add(new Employee() { FullName = "武汉办", PinYin = "whb", HotLine = "027-82668990", InternalLine = "" });
			elb.Items.Add(new Employee() { FullName = "东北办", PinYin = "dbb", HotLine = "024-22812660", InternalLine = "" });
			elb.Items.Add(new Employee() { FullName = "西安办", PinYin = "xab", HotLine = "029-86698900", InternalLine = "" });
			view = CollectionViewSource.GetDefaultView(elb.Items);

		}
		#endregion
		#region System EventHandler
		public void Window_Loaded(object sender, System.Windows.RoutedEventArgs e)
		{
			//Settings.Default.Reset();
			//this.Topmost = true;
			try
			{
				var temp = string.Concat(Environment.CurrentDirectory, @"\Log\");
				if (!System.IO.Directory.Exists(temp))
				{
					// Output directory does not exist, so create it.
					System.IO.Directory.CreateDirectory(temp);
				}
				using (var fs = new FileStream(string.Concat(Environment.CurrentDirectory, @"\Log\", DateTime.Now.ToLongDateString(), ".txt"), FileMode.OpenOrCreate))
				{
					using (var sr = new StreamReader(fs))
					{
						string line = string.Empty;
						List<string> lines = new List<string>();
						while ((line = sr.ReadLine()) != null)
						{
							lines.Add(line);
						}
						foreach (string s in lines)
						{
							log.Insert(0, new CallItem(s));
						}
					}
				}
				//using (StreamReader sr = new StreamReader("通道文件.txt"))
				//{
				//	channel.Text = sr.ReadLine();
				//	var t = sr.ReadLine();
				//	switch (t)
				//	{
				//		case "1":
				//			r1.IsChecked = true;
				//			break;
				//		case "2":
				//			r2.IsChecked = true;
				//			break;
				//		case "3":
				//			r3.IsChecked = true;
				//			break;
				//		default:
				//			r1.IsChecked = true;
				//			break;
				//	}
				//	sr.Close();
				//}
				var selectedBrowser = Settings.Default["SelectedBrowser"].ToString();
				switch (selectedBrowser)
				{
					case "1":
						r1.IsChecked = true;
						break;
					case "2":
						r2.IsChecked = true;
						break;
					case "3":
						r3.IsChecked = true;
						break;
					default:
						r1.IsChecked = true;
						break;
				}
				r1.Checked += r1_Checked;
				r2.Checked += r2_Checked;
				r3.Checked += r3_Checked;
				channel.Text = Settings.Default["ChannelCode"].ToString();
				if (caller != null) return;
				caller = new AxCTILink();
				WindowsFormsHost host = new WindowsFormsHost();
				caller.ComeInCallerID += caller_ComeInCallerID;
				caller.RobCallerID += caller_RobCallerID;
				caller.PickUp += caller_PickUp;
				caller.HangUp += caller_HangUp;
				caller.ConnectFailed += caller_ConnectFailed;
				//caller.ReceiveAuthMsg += caller_ReceiveAuthMsg;
				//caller.Ringing += caller_Ringing;

				//caller.CallOutCallerID += caller_CallOutCallerID;
				((System.ComponentModel.ISupportInitialize)(caller)).BeginInit();
				caller.OcxState = ((System.Windows.Forms.AxHost.State)(Properties.Resources.oo));
				host.Child = caller;
				this.Tag = host;
				((System.ComponentModel.ISupportInitialize)(caller)).EndInit();
				timer.Interval = 780000;
				timer.Elapsed += new ElapsedEventHandler(OnTimedEvent);
				timer.Start();
				hk = new HotKey(ModifierKeys.Control, System.Windows.Forms.Keys.Q, this);
				hk2 = new HotKey(ModifierKeys.Control, System.Windows.Forms.Keys.D1, this);
				hk.HotKeyPressed += hk_HotKeyPressed;
				hk2.HotKeyPressed += hk2_HotKeyPressed;
				ConnectServer();
			}
			catch (System.Exception)
			{
				var w = new Window1();
				
				w.ShowDialog();
				//MessageBox.Show("控件未注册" + Environment.NewLine + "下面将尝试自动注册并且关闭MiniCaller，注册成功之后请重新运行", "Error");
				System.Diagnostics.Process.Start("注册来电ocx.bat");
				//System.Windows.Forms.Application.Restart();
				Application.Current.Shutdown();

			}
		}
		

		void caller_ConnectFailed(object sender, EventArgs e)
		{
			MessageBox.Show("连接来电服务器失败！\r\n是否忘了修改通道号？双击通道号修改");
		}

		void hk2_HotKeyPressed(HotKey obj)
		{
			if (this.WindowState != System.Windows.WindowState.Normal) this.WindowState = System.Windows.WindowState.Normal;
		}

		//void caller_Ringing(object sender, EventArgs e)
		//{
		//	MessageBox.Show("ringing");
		//}

		//void caller_ReceiveAuthMsg(object sender, _DCTILinkEvents_ReceiveAuthMsgEvent e)
		//{
		//	MessageBox.Show(e.msg, "Received Message");
		//}

		//void caller_CallOutCallerID(object sender, _DCTILinkEvents_CallOutCallerIDEvent e)
		//{
		//	MessageBox.Show(string.Format("cid:{0}  cONTID:{1}  cr_id:{2} ", e.cID, e.cONTID, e.cr_id));
		//}
		protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
		{
			if (caller != null)
			{
				try
				{
					using (var t = new StreamWriter(string.Concat(Environment.CurrentDirectory, @"\Log\", DateTime.Now.ToLongDateString(), ".txt"), true))
					{
						foreach (var item in log.Reverse())
						{
							if (item.CallTime.Ticks < startup.Ticks) continue;
							t.WriteLine(item.ToString());
						}
					}
					caller.DisconnectServer();
					caller.Dispose();
				}
				catch (Exception)
				{
				}
			}
			base.OnClosing(e);
		}
		private void OnTimedEvent(object sender, ElapsedEventArgs e)
		{
			this.Dispatcher.BeginInvoke((Action)(() => this.ConnectServer()));
		}
		#endregion
		#region Caller EventHandler
		void caller_RobCallerID(object sender, _DCTILinkEvents_RobCallerIDEvent e)
		{
			caller_ComeInCallerID(null, new AxCTILINKLib._DCTILinkEvents_ComeInCallerIDEvent(e.cID, ""));
			log[0].PickedUpTime = DateTime.Now;
			log[0].PickedUp = true;
		}
		private void caller_HangUp(object sender, _DCTILinkEvents_HangUpEvent e)
		{
			pkup = false;
			rob.IsEnabled = false;
			if (logPtr == null) return;
			if (!logPtr.Equals(log[0])) { logPtr = null; return; }
			log[0].HangUpTime = DateTime.Now;
			log[0].CallDuration = log[0].HangUpTime - log[0].PickedUpTime;

		}

		private void caller_PickUp(object sender, _DCTILinkEvents_PickUpEvent e)
		{
			pkup = true;
			rob.IsEnabled = true;
			if (logPtr == null) return;
			if (!logPtr.Equals(log[0])) { logPtr = null; return; }
			log[0].PickedUp = true;
			log[0].PickedUpTime = DateTime.Now;
		}
		private void caller_ComeInCallerID(object sender, _DCTILinkEvents_ComeInCallerIDEvent e)
		{
			string pn = "";
			log.Insert(0, new CallItem() { PickedUp = false });
			logPtr = log[0];
			log[0].CallTime = DateTime.Now;
			log[0].PhoneNumber = regex.IsMatch(e.cID) ? e.cID : "未知号码";
			if (!regex.IsMatch(e.cID))
			{
				MessageBox.Show("未知号码，弹屏取消！");
				return;
			}
			try
			{
				pn = e.cID;
				if (e.cID.StartsWith("00") || e.cID.StartsWith("013") || e.cID.StartsWith("018") || e.cID.StartsWith("015"))
				{
					pn = e.cID.Substring(1);
					log[0].PhoneNumber = pn;
				}
				if (r1.IsChecked.Value)
				{
					Process.Start("iexplore.exe", "https://ap1.salesforce.com/_ui/search/ui/UnifiedSearchResults?searchType=2&str=" + pn);
				}
				else if (r2.IsChecked.Value)
				{
					Process.Start("chrome.exe", "https://ap1.salesforce.com/_ui/search/ui/UnifiedSearchResults?searchType=2&str=" + pn);
				}
				else if (r3.IsChecked.Value)
				{
					Process.Start("firefox.exe", "https://ap1.salesforce.com/_ui/search/ui/UnifiedSearchResults?searchType=2&str=" + pn);
				}

			}
			catch (Exception)
			{
				Process.Start("https://ap1.salesforce.com/_ui/search/ui/UnifiedSearchResults?searchType=2&str=" + pn);
			}
		}
		#endregion
		#region Private Field and Helpers
		void hk_HotKeyPressed(HotKey obj)
		{
			if (pkup)
				caller.RobAccept();
			else
			{
				this.WindowState = System.Windows.WindowState.Normal;
				this.Activate();
			}
		}
		private void ConnectServer()
		{
			//caller.ConnectToServer("192.168.100.240", 56899, "administrator", int.Parse(channel.Text));
		}



		private void add_phone(object sender, RoutedEventArgs e)
		{
			caller_ComeInCallerID(null, new _DCTILinkEvents_ComeInCallerIDEvent("0057188050332", null));
			log.Insert(0, new CallItem() { PickedUp = false, CallDuration = new TimeSpan(0, 5, 12), CallTime = DateTime.Now, PhoneNumber = "119" });
		}
		#endregion



		private void lb_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.Key == Key.C && Keyboard.Modifiers == ModifierKeys.Control)
			{
				if (lb.SelectedItem != null)
				{
					Clipboard.SetData(DataFormats.Text, (lb.SelectedItem as CallItem).PhoneNumber);
					//MessageBox.Show("电话号码已复制！","Info");
				}
			}
		}

		private void r1_Checked(object sender, RoutedEventArgs e)
		{
			//using (var t = new StreamWriter("通道文件.txt"))
			//{
			//	t.WriteLine(channel.Text);
			//	t.WriteLine("1");
			//	t.Flush();
			//	t.Close();
			//}
			Settings.Default["SelectedBrowser"] = 1;
			Settings.Default.Save();
		}

		private void r2_Checked(object sender, RoutedEventArgs e)
		{
			Settings.Default["SelectedBrowser"] = 2;
			Settings.Default.Save();
		}

		private void r3_Checked(object sender, RoutedEventArgs e)
		{
			Settings.Default["SelectedBrowser"] = 3;
			Settings.Default.Save();
		}
		private void channel_preview_input(object sender, TextCompositionEventArgs e)
		{
			Regex regex = new Regex(@"^\d$");
			e.Handled = !regex.IsMatch(e.Text);
			if (channel.Text.Length > 2) e.Handled = true;
		}


		private void rob_click(object sender, RoutedEventArgs e)
		{
			if (pkup)
				caller.RobAccept();
		}

		private void channel_double_click(object sender, MouseButtonEventArgs e)
		{
			if (e.ClickCount >= 2)
			{
				channel.IsEnabled = true;
				channel.Focus();
			}
		}



		private void channel_keydown(object sender, KeyEventArgs e)
		{
			if (e.Key == Key.Enter)
			{
				channel.IsEnabled = false;
				//using (var s = new StreamWriter("通道文件.txt"))
				//{
				//	s.WriteLine(channel.Text);
				//	if (r1.IsChecked.Value == true)
				//		s.WriteLine("1");
				//	else if (r2.IsChecked.Value == true)
				//		s.WriteLine("2");
				//	else if (r3.IsChecked.Value == true)
				//		s.WriteLine("3");
				//	s.Flush();
				//	s.Close();
				//}
				Settings.Default["ChannelCode"] = int.Parse(channel.Text);
				Settings.Default.Save();
				try
				{
					ConnectServer();
					System.Windows.MessageBox.Show("成功修改通道号并重新连接服务器!");
				}
				catch (Exception eee)
				{
					MessageBox.Show(eee.Message);
				}
			}
		}

		private void check_in_click(object sender, RoutedEventArgs e)
		{
			caller.CheckIn();
		}

		private void check_out_click(object sender, RoutedEventArgs e)
		{
			caller.CheckOut();
		}

		private void window_keydown(object sender, KeyEventArgs e)
		{
			if (e.Key == Key.F1)
				dalily_reply(null, null);
			else if (e.Key == Key.F2)
				close_query(null, null);
			else if (e.Key == Key.F5)
			{
				caller.CheckIn();
				MessageBox.Show("签入成功");
			}
			else if (e.Key == Key.F6)
			{
				caller.CheckOut();
				MessageBox.Show("签出成功");
			}
			else if (e.Key == Key.E && Keyboard.Modifiers == ModifierKeys.Control)
			{
				SearchTextBox.Focus();
			}
			else if (e.Key == Key.Escape)
			{
				//if (mailGrid.Visibility == System.Windows.Visibility.Visible)
				//{
				//	emailTb.Text = "dummy@dummy.com";
				//	//layoutRoot.Visibility = System.Windows.Visibility.Visible;
				//	//mailGrid.Visibility = System.Windows.Visibility.Collapsed;
				//	GridTransitionToLayoutRoot(mailGrid, layoutRoot);
				//	return;
				//}
				SearchTextBox.Text = string.Empty;
				//empGrid.Visibility = System.Windows.Visibility.Collapsed;
				//logGrid.Visibility = System.Windows.Visibility.Visible;
				if (empGrid.Visibility == System.Windows.Visibility.Visible)
					Fade(empGrid, logGrid);
				else if (logGrid.Visibility == System.Windows.Visibility.Visible)
				{
					this.WindowState = System.Windows.WindowState.Minimized;
				}

			}
		}
		//private bool IsEmailAllowed(string text)
		//{
		//	bool blnValidEmail = false;
		//	Regex regEMail = new Regex(@"^[a-zA-Z][\w\.-]{2,28}[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$");
		//	if (text.Length > 0)
		//	{
		//		blnValidEmail = regEMail.IsMatch(text);
		//	}

		//	return blnValidEmail;
		//}
		private void StackPanel_MouseDown(object sender, MouseButtonEventArgs e)
		{
			if (e.ClickCount >= 2)
			{
				try
				{
					if (r1.IsChecked.Value)
					{
						Process.Start("iexplore.exe", "https://ap1.salesforce.com/_ui/search/ui/UnifiedSearchResults?searchType=2&str=" + (lb.SelectedItem as CallItem).PhoneNumber);
					}
					else if (r2.IsChecked.Value)
					{
						Process.Start("chrome.exe", "https://ap1.salesforce.com/_ui/search/ui/UnifiedSearchResults?searchType=2&str=" + (lb.SelectedItem as CallItem).PhoneNumber);
					}
					else if (r3.IsChecked.Value)
					{
						Process.Start("firefox.exe", "https://ap1.salesforce.com/_ui/search/ui/UnifiedSearchResults?searchType=2&str=" + (lb.SelectedItem as CallItem).PhoneNumber);
					}
				}
				catch (Exception)
				{
					Process.Start("https://ap1.salesforce.com/_ui/search/ui/UnifiedSearchResults?searchType=2&str=" + (lb.SelectedItem as CallItem).PhoneNumber);
				}
			}
		}

		private void help_click(object sender, RoutedEventArgs e)
		{
			//MessageBox.Show("1, Ctrl + C  复制当前选中记录的电话号码" + Environment.NewLine + "2, Ctrl + Q    摘机状态：抢接电话； 挂机状态，激活窗口" + Environment.NewLine +
			//	 "3, 双击记录，弹出搜索窗口" + Environment.NewLine + "4, 输入拼音搜索， Esc 取消" + Environment.NewLine + "5, Ctrl + 1   激活窗口" + Environment.NewLine + "6, F1 复制日常回复模版到剪贴板， F2 复制关闭问询到剪贴板" + Environment.NewLine + "7, 常规状态下Esc 最小化到任务栏" + Environment.NewLine + "8, 邮件界面支持拖拽添加附件", "Help");
			this.Flyouts[0].IsOpen = !this.Flyouts[0].IsOpen;
		}

		private void Fade(UIElement from, UIElement to)
		{
			fromGrid = from;
			toGrid = to;
			if (from.Equals(logGrid))
			{
				to.Visibility = System.Windows.Visibility.Visible;
				to.Opacity = 0;
				from.Opacity = 1;
				from.Visibility = System.Windows.Visibility.Visible;
				ag = to;
			}
			else
			{
				to.Visibility = System.Windows.Visibility.Visible;
				to.Opacity = 0;
				from.Opacity = 1;
				from.Visibility = System.Windows.Visibility.Visible;
				ag = from;
			}
			CompositionTarget.Rendering += CompositionTarget_Rendering;
		}

		private void FlashTextBlock(UIElement t1, UIElement t2)
		{
			t1.Visibility = System.Windows.Visibility.Collapsed;
			t1.Opacity = 0;
			t2.Visibility = System.Windows.Visibility.Visible;
			t2.Opacity = 0.7;
			CompositionTarget.Rendering += FlashTextBlockTransitition;

		}

		void FlashTextBlockTransitition(object sender, EventArgs e)
		{
			if (!notifyTextBlockFadeDirection)
			{
				clipNotify.Opacity += 0.01;
				if (clipNotify.Opacity >= 1) notifyTextBlockFadeDirection = true;
			}
			else
			{
				clipNotify.Opacity -= 0.008;
				if (clipNotify.Opacity <= 0)
				{
					notifyTextBlockFadeDirection = false;
					dummy.Visibility = System.Windows.Visibility.Visible;
					dummy.Opacity = 1;
					clipNotify.Visibility = System.Windows.Visibility.Collapsed;
					clipNotify.Opacity = 0;
					CompositionTarget.Rendering -= FlashTextBlockTransitition;

				}
			}

		}

		void GridTransitionToMailGrid(UIElement t1, UIElement t2)
		{
			t1.Visibility = System.Windows.Visibility.Visible;
			t1.Opacity = 1;
			t2.Visibility = System.Windows.Visibility.Visible;
			t2.Opacity = 0.0;
			CompositionTarget.Rendering += GridTransitionHandler;
		}

		private void GridTransitionHandler(object sender, EventArgs e)
		{
			//layoutRoot.Opacity -= 0.07;
			//mailGrid.Opacity += 0.07;
			//if (mailGrid.Opacity >= 1)
			//{
			//	layoutRoot.Visibility = System.Windows.Visibility.Collapsed;
			//	layoutRoot.Opacity = 0;
			//	mailGrid.Visibility = System.Windows.Visibility.Visible;
			//	mailGrid.Opacity = 1;
			//	CompositionTarget.Rendering -= GridTransitionHandler;
			//}
		}

		void GridTransitionToLayoutRoot(UIElement t1, UIElement t2)
		{
			t1.Visibility = System.Windows.Visibility.Visible;
			t1.Opacity = 1;
			t2.Visibility = System.Windows.Visibility.Visible;
			t2.Opacity = 0.0;
			CompositionTarget.Rendering += GridTransitionToLayoutRootHandler;
		}

		private void GridTransitionToLayoutRootHandler(object sender, EventArgs e)
		{
			//layoutRoot.Opacity += 0.07;
			//mailGrid.Opacity -= 0.07;
			//if (layoutRoot.Opacity >= 1)
			//{
			//	mailGrid.Visibility = System.Windows.Visibility.Collapsed;
			//	mailGrid.Opacity = 0;
			//	layoutRoot.Visibility = System.Windows.Visibility.Visible;
			//	layoutRoot.Opacity = 1;
			//	CompositionTarget.Rendering -= GridTransitionToLayoutRootHandler;
			//}
		}

		private void CompositionTarget_Rendering(object sender, EventArgs e)
		{
			double op = -1;
			if (ag.Equals(fromGrid))
			{
				op = Math.Max(0, fromGrid.Opacity - .05);
				fromGrid.Opacity = op;
				toGrid.Opacity += 0.05;
			}
			else
			{
				op = Math.Min(1, toGrid.Opacity + .05);
				toGrid.Opacity = op;
				fromGrid.Opacity -= .05;
			}
			if (op == 1 || op == 0)
			{
				fromGrid.Opacity = 0;
				fromGrid.Visibility = System.Windows.Visibility.Collapsed;
				CompositionTarget.Rendering -= CompositionTarget_Rendering;
			}

		}

		private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
		{
			if (SearchTextBox.Text.Length == 0) { view.Filter = null; return; }
			else if (empGrid.Visibility == System.Windows.Visibility.Collapsed)
			{
				//empGrid.Visibility = System.Windows.Visibility.Visible;
				//logGrid.Visibility = System.Windows.Visibility.Collapsed;
				Fade(logGrid, empGrid);
			}
			switch (Encoding.Default.GetBytes(SearchTextBox.Text[0].ToString()).Length)
			{
				case 1:
					view.Filter = (c) =>
					{
						if ((c as Employee).PinYin.IndexOf(SearchTextBox.Text, 0, StringComparison.CurrentCultureIgnoreCase) > -1) return true;
						else return false;
					};
					break;
				default:
					view.Filter = (c) =>
					{
						if ((c as Employee).FullName.IndexOf(SearchTextBox.Text, 0, StringComparison.CurrentCultureIgnoreCase) > -1) return true;
						else return false;
					};
					break;
			}
		}

		private void close_click(object sender, RoutedEventArgs e)
		{
			SearchTextBox.Text = string.Empty;
			//empGrid.Visibility = System.Windows.Visibility.Collapsed;
			//logGrid.Visibility = System.Windows.Visibility.Visible;
			Fade(empGrid, logGrid);
			//caller.SendMsg(30, "8022");
		}



		private void search_keydown(object sender, KeyEventArgs e)
		{
			if (e.Key == Key.Escape)
			{
				SearchTextBox.Text = string.Empty;
				//empGrid.Visibility = System.Windows.Visibility.Collapsed;
				//logGrid.Visibility = System.Windows.Visibility.Visible;
				if (empGrid.Visibility == System.Windows.Visibility.Visible)
					Fade(empGrid, logGrid);
				else if (logGrid.Visibility == System.Windows.Visibility.Visible)
				{
					this.WindowState = System.Windows.WindowState.Minimized;
				}
			}
		}

		private void search_gotfocus(object sender, RoutedEventArgs e)
		{
			focused = true;
			//empGrid.Visibility = System.Windows.Visibility.Visible;
			//logGrid.Visibility = System.Windows.Visibility.Collapsed;
			Fade(logGrid, empGrid);
		}

		private void search_lostfocus(object sender, RoutedEventArgs e)
		{
			focused = false;
			SearchTextBox.Text = string.Empty;
			//empGrid.Visibility = System.Windows.Visibility.Collapsed;
			//logGrid.Visibility = System.Windows.Visibility.Visible;
		}

		AxCTILink caller;
		Timer timer = new Timer();
		ObservableCollection<CallItem> log = new ObservableCollection<CallItem>();
		Regex regex = new Regex(@"^\d+$");
		CallItem logPtr;
		HotKey hk;
		HotKey hk2;
		bool pkup = false;
		bool focused = false;
		ICollectionView view;
		readonly DateTime startup;
		UIElement fromGrid;
		UIElement toGrid;
		UIElement ag;
		bool notifyTextBlockFadeDirection = false;
		List<string> attachNames = new List<string>();
		//List<byte[]> attachStream = new List<byte[]>();
		bool HasIncidentNumber = false;
		bool IsSending = false;
		private void Window_PreviewKeyDown(object sender, KeyEventArgs e)
		{
			if (e.Key >= Key.A && e.Key <= Key.Z && Keyboard.Modifiers == ModifierKeys.None)
			{
				SearchTextBox.Focus();
			}
		}

		private void close_query(object sender, RoutedEventArgs e)
		{
			//var s = "您好！" + Environment.NewLine + "您的编号为 XXXXX 的问题是否已经得到解决？如果在3个工作日内没有进一步的反馈信息，我们将关闭这个问题。" + Environment.NewLine + "感谢您对我们产品的支持！" + Environment.NewLine + Environment.NewLine + "此致" + Environment.NewLine + "       敬礼！";
			//Clipboard.SetData(DataFormats.UnicodeText, s);
			var s = @"您好！

您的编号为XXXXXXXX的问题是否已经得到解决？如果在3个工作日内没有进一步的反馈信息，我们将关闭这个问题。

感谢您对我们产品的支持！

此致
      敬礼！


客户与合作伙伴支持部
分享地理价值

Esri中国信息技术有限公司
地址：北京市东城区东直门南大街甲3号居然大厦19层
邮编：100007
传真：010-57632299
技术支持热线：400-819-2881
Email：support@esrichina.com.cn
网址：www.esrichina.com.cn
ArcGIS资源中心：http://resources.arcgis.com/zh-cn";
			Clipboard.SetData(DataFormats.UnicodeText, s);
			FlashTextBlock(dummy, clipNotify);

		}

		private void dalily_reply(object sender, RoutedEventArgs e)
		{
			//var s = "XXX， 您好：" + Environment.NewLine + "问题解答：" + Environment.NewLine + Environment.NewLine + Environment.NewLine + Environment.NewLine + Environment.NewLine + Environment.NewLine + Environment.NewLine + "祝工作顺利，生活愉快！" + Environment.NewLine + Environment.NewLine + "Esri中国信息技术有限公司 " + "客户与合作伙伴支持部";
			var t = @"您好：

问题解答：





祝工作顺利，生活愉快！

客户与合作伙伴支持部

分享地理价值
Esri中国信息技术有限公司
地址：北京市东城区东直门南大街甲3号居然大厦19层
邮编：100007
传真：010-57632299
技术支持热线：400-819-2881
Email：support@esrichina.com.cn
网址：www.esrichina.com.cn
ArcGIS资源中心：http://resources.arcgis.com/zh-cn";
			Clipboard.SetData(DataFormats.UnicodeText, t);
			FlashTextBlock(dummy, clipNotify);

		}

		private void mail_click(object sender, RoutedEventArgs e)
		{
			//layoutRoot.Visibility = System.Windows.Visibility.Collapsed;
			//mailGrid.Visibility = System.Windows.Visibility.Visible;


			//GridTransitionToMailGrid(layoutRoot, mailGrid);
			//emailTb.Text = string.Empty;
			tcc.Content = Resources["mg"] as Grid;
		}

		private void clearAttach_Click(object sender, RoutedEventArgs e)
		{
			//attachCb.Items.Clear();
			////attachStream.Clear();
			//attachNames.Clear();
			//attachSizeTextblock.Text = string.Empty;
			//GC.Collect();
		}

		private void incidentTb_PreviewTextInput(object sender, TextCompositionEventArgs e)
		{
			Regex regex = new Regex(@"^\d$");
			e.Handled = !regex.IsMatch(e.Text);
			//if (incidentTb.Text.Length > 7) e.Handled = true;
		}

		private void addAttach(object sender, RoutedEventArgs e)
		{
			Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
			dlg.Multiselect = true;
			//var totalSize = 0.0;
			if (dlg.ShowDialog().Value)
			{

				var t = dlg.OpenFiles();
				//attachSafeName = dlg.SafeFileNames;
				//attachNames.AddRange(dlg.FileNames);
				//attachStream.Clear();
				for (int i = 0; i < t.Count(); i++)
				{
					if (attachNames.Contains(dlg.FileNames[i]))
					{
						continue;
					}
					var dsize = (double)t[i].Length / 1024 / 1024;
					var size = dsize.ToString("F01");
					if (size == "0.0") size = "0.1";
					//totalSize += dsize;
					//BinaryReader br = new BinaryReader(t[i]);
					//Byte[] bytes = br.ReadBytes((Int32)t[i].Length);
					//attachStream.Insert(0, bytes);
					//attachCb.Items.Insert(0,new ComboBoxItem() { Content = dlg.SafeFileNames[i] + "     " + size + "m", Tag = new MemoryStream(bytes) });
					//attachCb.Items.Insert(0, new AttachFile() { FileName = dlg.SafeFileNames[i], FileSize = size + "m", FileBytes = t[i], Size = dsize });
					attachNames.Insert(0, dlg.FileNames[i]);

				}
				double totalSize = 0;
				//foreach (var item in attachCb.Items)
				//{
				//	totalSize += (item as AttachFile).Size;
				//}
				//attachSizeTextblock.Text = string.Format("共{0}个文件，总大小：{1}m", attachCb.Items.Count, totalSize.ToString("F01"));
				//attachCb.SelectedIndex = 0;

			}
		}

		private void mailGrid_Drop(object sender, DragEventArgs e)
		{
			if (e.Data.GetDataPresent(DataFormats.FileDrop))
			{
				// Note that you can have more than one file.
				string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
				foreach (var item in files)
				{
					if (attachNames.Contains(item))
					{
						continue;
					}
					//MemoryStream ms = new MemoryStream();

					FileStream file = new FileStream(item, FileMode.Open, FileAccess.Read);


					var dsize = (double)file.Length / 1024 / 1024;
					var size = dsize.ToString("F01");
					if (size == "0.0") size = "0.1";
					//attachCb.Items.Insert(0, new AttachFile() { FileName = item.Substring(item.LastIndexOf("\\") + 1), FileSize = size + "m", FileBytes = file, Size = dsize });
					attachNames.Insert(0, item);



				}
				double totalSize = 0;
				//foreach (var item in attachCb.Items)
				//{
				//	totalSize += (item as AttachFile).Size;
				//}
				//attachSizeTextblock.Text = string.Format("共{0}个文件，总大小：{1}m", attachCb.Items.Count, totalSize.ToString("F01"));
				//attachCb.SelectedIndex = 0;
				// Assuming you have one file that you care about, pass it off to whatever
				// handling code you have defined.
				//HandleFileOpen(files[0]);
			}
		}

		private string GetResourceTextFile(string filename)
		{
			string result = string.Empty;

			using (Stream stream = this.GetType().Assembly.GetManifestResourceStream("MiniCaller.Resources." + filename))
			{
				using (StreamReader sr = new StreamReader(stream))
				{
					result = sr.ReadToEnd();
				}
			}
			return result;
		}

		private Stream GetLogo(string name)
		{
			return this.GetType().Assembly.GetManifestResourceStream("MiniCaller.Resources." + name);
		}

		private void send_click(object sender, RoutedEventArgs e)
		{
			////sendButton.IsEnabled = false;
			////attachCb.Items.Clear();
			//if (IsSending) return;
			//IsSending = true;
			//HasIncidentNumber = incidentTb.Text == "" ? false : true;
			//BusyIndicator.Visibility = System.Windows.Visibility.Visible;
			//var html = GetResourceTextFile("kkk.html").Replace("（对应问题编号#）", HasIncidentNumber ? "（对应问题编号#" + incidentTb.Text + "）" : "");
			//AlternateView avHtml = AlternateView.CreateAlternateViewFromString(html, null, MediaTypeNames.Text.Html);
			//LinkedResource pic1 = new LinkedResource(GetLogo("mail_line.jpg"), MediaTypeNames.Image.Jpeg);
			//pic1.ContentId = "pic1";
			//avHtml.LinkedResources.Add(pic1);
			//LinkedResource pic2 = new LinkedResource(GetLogo("esri_logo.jpg"), MediaTypeNames.Image.Jpeg);
			//pic2.ContentId = "pic2";
			//avHtml.LinkedResources.Add(pic2);
			//MailMessage m = new MailMessage();
			//SmtpClient client = new SmtpClient();
			//m.AlternateViews.Add(avHtml);
			//client.Host = "smtp.esrichina.com.cn";
			////client.Host = "smtp.163.com";
			//client.Port = 25;
			//client.DeliveryMethod = SmtpDeliveryMethod.Network;
			//client.UseDefaultCredentials = false;
			//client.Credentials = new System.Net.NetworkCredential("support@esrichina.com.cn", "Esrichina123");
			////client.Credentials = new System.Net.NetworkCredential("crazyxhz", "xhzesee86");
			////m.From = new MailAddress("crazyxhz@163.com");
			//m.From = new MailAddress("support@esrichina.com.cn");
			//m.To.Add(new MailAddress(emailTb.Text));
			//m.Subject = HasIncidentNumber ? "Esri中国技术支持 附件专用邮件，问题编号#" + incidentTb.Text + ",请勿回复" : "Esri中国技术支持 附件专用邮件,请勿回复";
			//for (int i = 0; i < attachCb.Items.Count; i++)
			//{
			//	Attachment data = new Attachment((attachCb.Items[i] as AttachFile).FileBytes, attachNames[i].Substring(attachNames[i].LastIndexOf("\\") + 1));
			//	m.Attachments.Add(data);
			//}
			//client.SendCompleted += client_SendCompleted;
			//try
			//{
			//	client.SendAsync(m, null);
			//}
			//catch (Exception mailException)
			//{
			//	IsSending = false;
			//	MessageBox.Show(mailException.Message, "发送邮件失败");
			//}
		}

		void client_SendCompleted(object sender, AsyncCompletedEventArgs e)
		{
			//throw new NotImplementedException();
			//IsSending = false;
			//BusyIndicator.Visibility = System.Windows.Visibility.Collapsed;
			//emailTb.Text = string.Empty;
			//incidentTb.Text = string.Empty;
			//clearAttach_Click(null, null);
			//attachSizeTextblock.Text = "发送成功";
		}

		private void emailTb_GotKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
		{
			//if (attachCb.Items.IsEmpty)
			//	attachSizeTextblock.Text = "";
		}

		//private void emailTb_PreviewLostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
		//{
		//	if (IsEmailAllowed(emailTb.Text.Trim()) == false)
		//	{
		//		e.Handled = true;
		//		MessageBox.Show("Email 地址不合法！", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
		//		emailTb.Focus();

		//	}
		//}
	}
}
