﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Data;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Interop;

namespace MiniCaller
{
	class CallItem : INotifyPropertyChanged
	{
		public DateTime CallTime
		{
			get { return _cT; }
			set { _cT = value; OnPropertyChanged("CallTime"); }
		}


		public bool PickedUp
		{
			get { return _pU; }
			set { _pU = value; OnPropertyChanged("PickedUp"); }
		}


		public string PhoneNumber
		{
			get { return _pN; }
			set { _pN = value; OnPropertyChanged("PhoneNumber"); }
		}


		public string IncidentName
		{
			get { return _iN; }
			set { _iN = value; OnPropertyChanged("IncidentName"); }
		}


		public TimeSpan CallDuration
		{
			get { return _cD; }
			set { _cD = value; OnPropertyChanged("CallDuration"); }
		}
		private DateTime _pickedTime;

		public DateTime PickedUpTime
		{
			get { return _pickedTime; }
			set { _pickedTime = value; OnPropertyChanged("PickedUpTime"); }
		}
		private DateTime _hangupTime;

		public DateTime HangUpTime
		{
			get { return _hangupTime; }
			set { _hangupTime = value; OnPropertyChanged("HangUpTime"); }
		}





		public string CompanyName
		{
			get { return _cN; }
			set { _cN = value; OnPropertyChanged("CompanyName"); }
		}

		private DateTime _cT;
		private string _cN;
		private TimeSpan _cD;
		private string _iN;
		private string _pN;
		private bool _pU;
		public override string ToString()
		{
			return CallTime.Ticks + " " + PickedUp + " " + PhoneNumber + " " + CallDuration.Ticks;
		}
		public event PropertyChangedEventHandler PropertyChanged;
		//private string s;
		public CallItem()
		{

		}
		public CallItem(string s)
		{
			var t = s.Split(new char[] { ' ' });
			CallTime = new DateTime(long.Parse(t[0]));
			PickedUp = bool.Parse(t[1]);
			PhoneNumber = t[2];
			CallDuration = new TimeSpan(long.Parse(t[3]));
		}
		private void OnPropertyChanged(string propertyName)
		{
			this.OnPropertyChanged(new PropertyChangedEventArgs(propertyName));
		}
		private void OnPropertyChanged(PropertyChangedEventArgs e)
		{
			var handler = this.PropertyChanged;
			if (handler != null)
			{
				handler(this, e);
			}
		}
	}
	class ShortDateConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			if (value is DateTime)
			{
				var t = (DateTime)value;
				string result = string.Empty;
				if (t.Month.ToString().Length == 1)
					result += "  " + t.Month;
				else
					result += t.Month.ToString();
				result += "月";
				if (t.Day.ToString().Length == 1)
				{
					result += "  " + t.Day.ToString();
				}
				else
					result += t.Day.ToString();
				result += "日";
				if (t.Hour.ToString().Length == 1)
				{
					result += "   " + t.Hour;
				}
				else
					result += " " + t.Hour.ToString();
				result += ":";
				if (t.Minute.ToString().Length == 1)
				{
					result += "0" + t.Minute;
				}
				else
					result += t.Minute;
				return result;
			}
			return value;
		}

		public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			return DependencyProperty.UnsetValue;
		}
	}
	class TimeSpanConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			if (value is TimeSpan)
			{
				var t = (TimeSpan)value;
				if ((int)t.TotalMinutes == 0) return t.Seconds + " 秒";
				else return (int)t.TotalMinutes + " 分 " + t.Seconds + " 秒";
			}
			return null;
		}

		public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
	internal class HotKeyWinApi
	{
		public const int WmHotKey = 0x0312;

		[DllImport("user32.dll", SetLastError = true)]
		public static extern bool RegisterHotKey(IntPtr hWnd, int id, ModifierKeys fsModifiers, Keys vk);

		[DllImport("user32.dll", SetLastError = true)]
		public static extern bool UnregisterHotKey(IntPtr hWnd, int id);
	}
	public sealed class HotKey : IDisposable
	{
		public event Action<HotKey> HotKeyPressed;

		private readonly int _id;
		private bool _isKeyRegistered;
		readonly IntPtr _handle;

		public HotKey(ModifierKeys modifierKeys, Keys key, Window window)
			: this(modifierKeys, key, new WindowInteropHelper(window))
		{
			//Contract.Requires(window != null);
		}

		public HotKey(ModifierKeys modifierKeys, Keys key, WindowInteropHelper window)
			: this(modifierKeys, key, window.Handle)
		{
			//Contract.Requires(window != null);
		}

		public HotKey(ModifierKeys modifierKeys, Keys key, IntPtr windowHandle)
		{
			//Contract.Requires(modifierKeys != ModifierKeys.None || key != Keys.None);
			//Contract.Requires(windowHandle != IntPtr.Zero);

			Key = key;
			KeyModifier = modifierKeys;
			_id = GetHashCode();
			_handle = windowHandle;
			RegisterHotKey();
			ComponentDispatcher.ThreadPreprocessMessage += ThreadPreprocessMessageMethod;
		}

		~HotKey()
		{
			Dispose();
		}

		public Keys Key { get; private set; }

		public ModifierKeys KeyModifier { get; private set; }

		public void RegisterHotKey()
		{
			if (Key == Keys.None)
				return;
			if (_isKeyRegistered)
				UnregisterHotKey();
			_isKeyRegistered = HotKeyWinApi.RegisterHotKey(_handle, _id, KeyModifier, Key);
			if (!_isKeyRegistered)
				throw new ApplicationException("Hotkey already in use");
		}

		public void UnregisterHotKey()
		{
			_isKeyRegistered = !HotKeyWinApi.UnregisterHotKey(_handle, _id);
		}

		public void Dispose()
		{
			ComponentDispatcher.ThreadPreprocessMessage -= ThreadPreprocessMessageMethod;
			UnregisterHotKey();
		}

		private void ThreadPreprocessMessageMethod(ref MSG msg, ref bool handled)
		{
			if (!handled)
			{
				if (msg.message == HotKeyWinApi.WmHotKey
						&& (int)(msg.wParam) == _id)
				{
					OnHotKeyPressed();
					handled = true;
				}
			}
		}

		private void OnHotKeyPressed()
		{
			if (HotKeyPressed != null)
				HotKeyPressed(this);
		}

	}

	public static class MinimizeToTray
	{
		/// <summary>
		/// Enables "minimize to tray" behavior for the specified Window.
		/// </summary>
		/// <param name="window">Window to enable the behavior for.</param>
		public static void Enable(Window window)
		{
			// No need to track this instance; its event handlers will keep it alive
			new MinimizeToTrayInstance(window);
		}

		/// <summary>
		/// Class implementing "minimize to tray" functionality for a Window instance.
		/// </summary>
		private class MinimizeToTrayInstance
		{
			private Window _window;
			private NotifyIcon _notifyIcon;
			private bool _balloonShown;

			/// <summary>
			/// Initializes a new instance of the MinimizeToTrayInstance class.
			/// </summary>
			/// <param name="window">Window instance to attach to.</param>
			public MinimizeToTrayInstance(Window window)
			{
				Debug.Assert(window != null, "window parameter is null.");
				_window = window;
				_window.StateChanged += new EventHandler(HandleStateChanged);
			}

			/// <summary>
			/// Handles the Window's StateChanged event.
			/// </summary>
			/// <param name="sender">Event source.</param>
			/// <param name="e">Event arguments.</param>
			private void HandleStateChanged(object sender, EventArgs e)
			{
				if (_notifyIcon == null)
				{
					// Initialize NotifyIcon instance "on demand"
					_notifyIcon = new NotifyIcon();
					_notifyIcon.Icon = Icon.ExtractAssociatedIcon(Assembly.GetEntryAssembly().Location);
					_notifyIcon.MouseClick += new System.Windows.Forms.MouseEventHandler(HandleNotifyIconOrBalloonClicked);
					//_notifyIcon.BalloonTipClicked += new EventHandler(HandleNotifyIconOrBalloonClicked);
					_notifyIcon.ContextMenu = new ContextMenu(new MenuItem[] { new MenuItem("退出", onClick) });
				}
				// Update copy of Window Title in case it has changed
				_notifyIcon.Text = _window.Title;

				// Show/hide Window and NotifyIcon
				var minimized = (_window.WindowState == WindowState.Minimized);
				_window.ShowInTaskbar = !minimized;
				_notifyIcon.Visible = minimized;
				if (minimized && !_balloonShown)
				{
					// If this is the first time minimizing to the tray, show the user what happened
					_notifyIcon.ShowBalloonTip(1000, null, _window.Title, ToolTipIcon.None);
					_balloonShown = true;
				}
			}

			private void onClick(object sender, EventArgs e)
			{
				_notifyIcon.Visible = false;
				_window.Close();
			}

			/// <summary>
			/// Handles a click on the notify icon or its balloon.
			/// </summary>
			/// <param name="sender">Event source.</param>
			/// <param name="e">Event arguments.</param>
			private void HandleNotifyIconOrBalloonClicked(object sender, System.Windows.Forms.MouseEventArgs e)
			{
				// Restore the Window
				if (e.Button == MouseButtons.Left)
					_window.WindowState = WindowState.Normal;
			}
		}
	}
	public class TextInputToVisibilityConverter : IMultiValueConverter
	{
		public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			if (values[0] is bool && values[1] is bool)
			{
				bool hasText = !(bool)values[0];
				bool hasFocus = (bool)values[1];

				if (hasFocus || hasText)
					return Visibility.Collapsed;
			}
			return Visibility.Visible;
		}
		public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
	public class Employee
	{
		public string FullName { get; set; }
		public string HotLine { get; set; }
		public string InternalLine { get; set; }
		public string PinYin { get; set; }
	}

	public class EmailAddress : IDataErrorInfo
	{
		public string Email { get; set; }
		public string Error
		{
			get { return null; }
		}

		public string this[string columnName]
		{
			get
			{
				if (columnName == "Email")
				{
					//Regex regEMail = new Regex(@"^[a-zA-Z][\w\.-]{2,28}[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$");
					Regex regEMail = new Regex(@"^[\w!#$%&'*+\-/=?\^_`{|}~]+(\.[\w!#$%&'*+\-/=?\^_`{|}~]+)*@((([\-\w]+\.)+[a-zA-Z]{2,4})|(([0-9]{1,3}\.){3}[0-9]{1,3}))$");
					if (!regEMail.IsMatch(this.Email)) return "Email地址不合法";
					return null;
				}
				return null;
			}
		}
	}

	public class IncidentNumber : IDataErrorInfo
	{
		public string Number { get; set; }
		public string Error
		{
			get { return null; }
		}

		public string this[string columnName]
		{
			get
			{
				if (columnName == "Number")
				{
					Regex regx = new Regex(@"^\d+$");
					if (Number == string.Empty) return null;
					if (!regx.IsMatch(this.Number)) return "编号非数字";
					if (Number.Length != 8) return "非8位编号";
					return null;
				}
				return null;
			}
		}
	}

	public class AttachFile
	{
		public string FileName { get; set; }
		public string FileSize { get; set; }
		public Stream FileBytes { get; set; }
		public double Size { get; set; }
	}
}
