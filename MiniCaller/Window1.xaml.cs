﻿using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MiniCaller
{
	/// <summary>
	/// Interaction logic for Window1.xaml
	/// </summary>
	public partial class Window1 : MetroWindow
	{
		public Window1()
		{
			InitializeComponent();
		}
		int c = 0;
		private void Window_Loaded(object sender, RoutedEventArgs e)
		{
			Timer t = new Timer();
			t.Interval = 1000;
			t.Elapsed += new ElapsedEventHandler(t_Elapsed);
			t.Start();

		}

		private void t_Elapsed(object sender, ElapsedEventArgs e)
		{
			c++;
			if (c > 4)
				this.Dispatcher.Invoke(new Action(() =>
				{
					this.Close();
				}), null);
			
			this.Dispatcher.Invoke(new Action(() =>
			{
				sec.Text = (5-c).ToString();
			}), null);
			//this.Close();

		}
	}
}
